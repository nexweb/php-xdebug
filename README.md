### About ###

This repository contains the docker file for PHP 7.1 w/ xdebug. It should contain everything we need to run unit / functional tests on a project.

### Supported tags and respective `Dockerfile` links ###

- [`7.2`, `latest`](https://bitbucket.org/nexweb/php-xdebug/src/7577f5b9517d79f53f1b02859d432f39cc9bcf3a/7.2/Dockerfile?at=master&fileviewer=file-view-default)
- [`7.1`](https://bitbucket.org/nexweb/php-xdebug/src/7577f5b9517d79f53f1b02859d432f39cc9bcf3a/7.1/Dockerfile?at=master&fileviewer=file-view-default)
- [`7.0`](https://bitbucket.org/nexweb/php-xdebug/src/7577f5b9517d79f53f1b02859d432f39cc9bcf3a/7.0/Dockerfile?at=master&fileviewer=file-view-default)

### What's included ###

- php
- unzip (to speed-up composer install)
- composer

And the following php modules:

- xdebug
- pdo_mysql
- curl
- zip
- dom
- gd
